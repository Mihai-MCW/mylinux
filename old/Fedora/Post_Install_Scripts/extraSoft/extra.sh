#!/usr/bin/bash
# Install Netbeans
#git clone https://github.com/apache/incubator-netbeans ~/Software/NetBeans
#cd ~/Software/NetBeans
# extra steps needed
#ant

#Install Visual Paradigm (GUI install) https://www.visual-paradigm.com/download/community.jsp?platform=linux&arch=64bit
wget https://www.visual-paradigm.com/downloads/vpce/Visual_Paradigm_CE_Linux64.sh -O ~/Downloads/visualParadigm.sh
sh ~/Downloads/visualParadigm.sh
