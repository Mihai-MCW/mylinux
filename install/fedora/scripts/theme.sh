#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# installs the theme: https://github.com/vinceliuice/matcha

# get the needed dependencies
sudo dnf install gtk-murrine-engine gtk2-engines gtk2-engines-devel gtk3-devel -y

# get the source
git clone https://github.com/vinceliuice/matcha.git ./tmp

# install the theme
sh ./tmp/Install

# clean up
rm -rf ./tmp

# set the theme
gsettings set org.gnome.desktop.interface gtk-theme Matcha-azul
