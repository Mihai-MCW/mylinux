#!/usr/bin/bash

# Ensure the dependency is installed
sudo dnf install npm

# Install angular
sudo npm install -g @angular/cli
