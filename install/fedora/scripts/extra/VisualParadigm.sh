#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

#Install Visual Paradigm (GUI install) https://www.visual-paradigm.com/download/community.jsp?platform=linux&arch=64bit
# make a temporary folder
mkdir ./tmp
# get the files in that tmp folder
wget https://www.visual-paradigm.com/downloads/vpce/Visual_Paradigm_CE_Linux64.sh -O ./tmp/visualParadigm.sh
# install the program
sh ./tmp/visualParadigm.sh
# clean up
rm -rf ./tmp
