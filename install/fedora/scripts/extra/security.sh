#!/usr/bin/bash

# need to make an automated way to install all libraries
sudo dnf install yasm yasm-devel mingw32-wxWidgets mingw64-wxWidgets mingw32-wxWidgets3 mingw64-wxWidgets3 pkgconf-pkg-config mingw32-pkg-config mingw64-pkg-config fuse-libs

wget https://www.visual-paradigm.com/downloads/vpce/Visual_Paradigm_CE_Linux64.sh -O ./tmp/visualParadigm.sh

https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.3/wxWidgets-3.1.3.tar.bz2

# Get VeraCrypt
git clone https://github.com/veracrypt/VeraCrypt.git ./tmp
