#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# Remove any previous versions installed
sudo dnf remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine

# Ensure the package allowing to manage repos is installed
sudo dnf install dnf-plugins-core -y

# Add the docker ComunityEdition repo
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

# install the latest version of Docker
sudo dnf install docker-ce docker-ce-cli containerd.io -y

# No more sudo for using docker
sudo groupadd docker
sudo usermod -a -G docker $USER

