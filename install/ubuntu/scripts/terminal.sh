#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# Custom shell prompt with aliases Source which I'm using for now: https://www.linuxquestions.org/questions/linux-general-1/ultimate-prompt-and-bashrc-file-4175518169/
# Some colors are not quite there for me and I'll probably build my own eventually

cat ./terminal/bashrc >> ~/.bashrc

# Add aliases to the command line
echo -e 'if [ -f ~/.bashrc_aliases ]; then\n. ~/.bashrc_aliases\nfi\n' >> ~/.bashrc
cat ./terminal/bashrc.aliases >> ~/.bashrc_aliases

# Add a color scheme to the command line
echo '# Use ls colors' >> ~/.bashrc
echo 'eval $(dircolors -b $HOME/.LS_COLORS)' >> ~/.bashrc
wget https://raw.githubusercontent.com/trapd00r/LS_COLORS/master/LS_COLORS -O ~/.LS_COLORS #this can be replaced with whatever color scheme is desired

# If ~./inputrc doesn't exist yet, first include the original /etc/inputrc so we don't override it
if [ ! -a ~/.inputrc ]; then echo '$include /etc/inputrc' > ~/.inputrc; fi
# Add option to ~/.inputrc to enable case-insensitive tab completion
echo 'set completion-ignore-case On' >> ~/.inputrc
